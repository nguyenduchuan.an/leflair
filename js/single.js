(function($, app) {

    var homeCls = function() {

        var vars = {};
        var ele = {};

        this.run = function() {
            this.init();
            this.bindEvents();
        };

        this.init = function() {
            vars.id = 0;
        };

        this.bindEvents = function() {

            initDetail();
            initShowtxt();



        };

        this.resize = function() {

        };

        var initDetail = function(){
            $(function () {
                //ZOOM
                $("#zoom1").glassCase({
                    'widthDisplay': 500,
                    'heightDisplay': 500,
                    'nrThumbsPerRow': 5,
                    'isSlowZoom': false,
                    'colorIcons': '#00a54f',
                    'colorActiveThumb': '#00a54f',
                    'thumbsPosition': 'left'
                });

            });
            var swiper = new Swiper('.swiper-detail', {
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
            });
        }

var initShowtxt = function(){
            $('.title-detail span').click(function () {
                $(this).toggleClass('open');
                $(this).parent().next().slideToggle();
            })
        }



    };


    $(document).ready(function() {
        var homeObj = new homeCls();
        homeObj.run();
        // On resize
        $(window).resize(function() {
            homeObj.resize();
            if ($(window).width() <= 768) {
                $('#sidenav').removeClass('main-menu');
            } else {
                $('#sidenav').addClass('main-menu');
            }
        });
    });
}(jQuery, $.app));
