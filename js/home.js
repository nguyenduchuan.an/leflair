(function($, app) {

    var homeCls = function() {

        var vars = {};
        var ele = {};

        this.run = function() {
            this.init();
            this.bindEvents();
        };

        this.init = function() {
            vars.id = 0;
        };

        this.bindEvents = function() {

            initMenu();
            initSlideBanner();
            initS1();
            initS2();
            initS3();
            initMH();
            initQuant();
            initPopup();
            initFilter();
            initSelect();
            initGotop();
            initShowFilter();
            initCollap();
        };

        this.resize = function() {

        };
        var initGotop = function() {
            $('.go-top').click(function () {
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;
            });
        }
        var initShowFilter = function() {
            var overlay = $('<div class="overlay"></div>');
            $('body').prepend(overlay);
            $('.btn-fliter').click(function () {
                $('.siderBar').toggleClass('open');
                overlay.show();
            })
            overlay.click(function () {
                $('.siderBar').removeClass('open');
                $(this).hide();
            })

        }

        var initQuant = function(){
            jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="images/home/plus.png" alt=""></div><div class="quantity-button quantity-down"><img src="images/home/minus.png" alt=""></div></div>').insertAfter('.quantity input');
            jQuery('.quantity').each(function () {
                var spinner = jQuery(this),
                    input = spinner.find('input[type="number"]'),
                    btnUp = spinner.find('.quantity-up'),
                    btnDown = spinner.find('.quantity-down'),
                    min = input.attr('min'),
                    max = input.attr('max');

                btnUp.click(function () {
                    var oldValue = parseFloat(input.val());
                    if (oldValue >= max) {
                        var newVal = oldValue;
                    } else {
                        var newVal = oldValue + 1;
                    }
                    spinner.find("input").val(newVal);
                    spinner.find("input").trigger("change");
                });

                btnDown.click(function () {
                    var oldValue = parseFloat(input.val());
                    if (oldValue <= min) {
                        var newVal = oldValue;
                    } else {
                        var newVal = oldValue - 1;
                    }
                    spinner.find("input").val(newVal);
                    spinner.find("input").trigger("change");
                });

            });
        }
        var initMenu = function() {
            $(".accodition").click(function () {
                $(this).next().slideToggle('fast');
                $(this).toggleClass('rotate');
            });
            $(".open-sidemenu").click(function () {
                $('#sidenav').toggleClass('show-menu');
                $('.block-overlay').toggleClass('over');
                $('body').toggleClass('no-scroll');
            });
            $(".open-cart").click(function () {
                $('.block-cart').toggleClass('show-cart');
                $('.block-over').toggleClass('over');
                $('body').toggleClass('no-scroll');
            });
            $(".block-overlay").click(function () {
                $('#sidenav').toggleClass('show-menu');
                $(this).toggleClass('over');
                $('body').toggleClass('no-scroll');
            });
            $(".block-over").click(function () {
                $('.block-cart').toggleClass('show-cart');
                $(this).toggleClass('over');
                $('body').toggleClass('no-scroll');
            });
            $(".close-cart").click(function () {
                $('.block-cart').toggleClass('show-cart');
                $('.block-over').toggleClass('over');
                $('body').toggleClass('no-scroll');
            });
        };

        var initSlideBanner = function() {
            var slideBanner = new Swiper('.swiper-banner',{
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        }
        var initFilter = function() {
            $('.title-sidebar').click(function () {
                $(this).next().slideToggle();
                $(this).toggleClass('open');

            })
        }
        var initCollap = function(){
            $('.collap-icon').click(function () {
                $('.cart-widget').toggleClass('open');
            })
        }




        var initS1 = function() {
            var swiper = new Swiper('.swiper-new', {
                slidesPerView: 6,
                spaceBetween: 20,
                loop: true,
                navigation: {
                  nextEl: '.swiper-button-next-n',
                  prevEl: '.swiper-button-prev-n',
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 2,
                        spaceBetween: 5
                    },
                    // when window width is >= 480px
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 5
                    },
                    // when window width is >= 640px
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 10
                    },
                    1200: {
                        slidesPerView: 6
                    }
                }
            });
        }
        var initS2 = function() {
            var swiper = new Swiper('.swiper-sale', {
                slidesPerView: 6,
                spaceBetween: 20,
                loop: true,
                navigation: {
                  nextEl: '.swiper-button-next-s',
                  prevEl: '.swiper-button-prev-s',
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 2,
                        spaceBetween: 5
                    },
                    // when window width is >= 480px
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 5
                    },
                    // when window width is >= 640px
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 10
                    },
                    1200: {
                        slidesPerView: 6
                    }
                }
            });
        }
        var initS3 = function() {
            var swiper = new Swiper('.swiper-popular', {
                slidesPerView: 6,
                spaceBetween: 20,
                loop: true,
                navigation: {
                  nextEl: '.swiper-button-next-p',
                  prevEl: '.swiper-button-prev-p',
                },
                breakpoints: {
                    // when window width is >= 320px
                    320: {
                        slidesPerView: 2,
                        spaceBetween: 5
                    },
                    // when window width is >= 480px
                    480: {
                        slidesPerView: 2,
                        spaceBetween: 5
                    },
                    // when window width is >= 640px
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 10
                    },
                    1200: {
                        slidesPerView: 6
                    }
                }
            });
        }

        var initMH = function() {
            // item
            $('.mh-banner').matchHeight();
            $('.mh-address').matchHeight();
            $('.mh-adIt').matchHeight();
        }
        var initPopup = function() {
            $('a.open-modal').click(function(event) {
                $(this).modal({
                    fadeDuration: 250
                });
                return false;
            });
        }
        var initSelect = function() {
            $('.js-select').select2();
        }
    };


    $(document).ready(function() {
        var homeObj = new homeCls();
        homeObj.run();
        // On resize
        $(window).resize(function() {
            homeObj.resize();
            if ($(window).width() <= 768) {
                $('#sidenav').removeClass('main-menu');
            } else {
                $('#sidenav').addClass('main-menu');
            }
        });
    });
}(jQuery, $.app));
